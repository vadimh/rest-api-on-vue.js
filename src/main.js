import '@babel/polyfill';
import 'mutationobserver-shim';
import Vue from 'vue';
import './plugins/bootstrap-vue';
import App from './App.vue';
import router from './router';
import ApiService from './services/api.service';

import './filters';

Vue.config.productionTip = false;

ApiService.init();

new Vue({
    router,
    render: h => h(App)
}).$mount('#app');
