import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        name: 'home',
        path: '/',
        component: () => import('@/views/Home')
    },
    {
        name: 'article',
        path: '/articles/:id',
        component: () => import('@/views/Article'),
        props: true
    },
    {
        name: 'article-edit',
        path: '/edit/:id',
        props: true,
        component: () => import('@/views/ArticleEdit')
    },
    {
        name: 'article-new',
        path: '/new',
        component: () => import('@/views/ArticleEdit')
    }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
