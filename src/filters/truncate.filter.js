export default (string, limit = 60) => {
    return string.slice(0, limit) + (limit < string.length ? '...' : '');
};
