/**
 * import and init global filters
 */

import Vue from 'vue';

import date from './date.filter';
import truncate from './truncate.filter';

Vue.filter('date', date);
Vue.filter('truncate', truncate);
