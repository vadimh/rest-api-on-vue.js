import ApiService from './api.service';

export const ArticlesService = {
    query(params) {
        return ApiService.query('articles', params);
    },

    get(id) {
        return ApiService.get('articles', id);
    },

    create(params) {
        return ApiService.post('articles', params);
    },

    update(id, params) {
        return ApiService.update('articles', id, params);
    },

    destroy(id) {
        return ApiService.delete(`articles/${id}`);
    }
};
