import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = process.env.VUE_APP_BASE_API;

        Vue.axios.interceptors.request.use(config => {
            config.headers['Authorization'] = `Bearer replace_to_your_token`;
            return config;
        });

        Vue.axios.interceptors.response.use(
            response => response,
            error => {
                switch (error.response.status) {
                    case 400:
                        console.error(error.response.status, error.message);
                        break;
                    case 401:
                        // authentication error handler
                        break;
                    default:
                        console.error(error.response.status, error.message);
                }

                return Promise.reject(error);
            }
        );
    },

    query(resource, params) {
        return Vue.axios.get(resource, params);
    },

    get(resource, id) {
        return Vue.axios.get(`${resource}/${id}`);
    },

    post(resource, params) {
        return Vue.axios.post(`${resource}`, params);
    },

    update(resource, id, params) {
        return Vue.axios.put(`${resource}/${id}`, params);
    },

    put(resource, params) {
        return Vue.axios.put(`${resource}`, params);
    },

    delete(resource) {
        return Vue.axios.delete(resource);
    }
};

export default ApiService;
